from __future__ import unicode_literals
from django.db import models
from django.forms import ModelForm
import psycopg2 as dbapi2
from datetime import datetime

# Create your models here.


class patient(models.Model):
    # id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=50)
    phone = models.CharField(max_length=15)
    date = models.DateTimeField(auto_now_add=True, blank=True)


    def __unicode__(self):
            return self.name

class doctor(models.Model):
    #id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    designation = models.CharField(max_length=100)

    def __unicode__(self):
            return self.name

class appointment(models.Model):
    # id = models.AutoField(primary_key=True)
    patientname = models.CharField(max_length=100)
    dctr = models.CharField(max_length=100)
    date = models.DateTimeField(auto_now_add=True, blank=True)


    def __unicode__(self):
            return self.name
