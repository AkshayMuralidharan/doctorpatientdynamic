from django.contrib import admin
from back.models import patient
from back.models import doctor

# Register your models here.
admin.site.register(patient)
admin.site.register(doctor)
