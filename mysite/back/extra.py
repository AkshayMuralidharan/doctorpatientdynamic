from django.shortcuts import render, redirect, get_object_or_404
import psycopg2
import json
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
import sys
import logging
import urlparse
import psycopg2 as dbapi2
from back.models import patient
from back.models import doctor
logger = logging.getLogger(__name__)

#from django.template.context import RequestContext
from .forms import patientForm
from .forms import doctorForm

#disabling csrf (cross site request forgery)
@csrf_exempt
def patientin(request):
    #if post request came
    if request.method == 'POST':


        #getting values from post
        name = request.POST.get('name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        form = patientForm(request.POST or None)
        if form.is_valid():
            #f = patient(name='name', email='email', phone='phone')
            f = form.save()

        context = {
            'name': name,
            'email': email,
            'phone': phone
        }


        template = loader.get_template('showdata.html')

        return HttpResponse(template.render(context, request))
    else:
        #if post request is not true
        #returing the form template
        template = loader.get_template('index.html')
        return HttpResponse(template.render())

def patientl_list(request):

    currentUrl = request.get_full_path()
    parsed = urlparse.urlparse(currentUrl)

    if urlparse.parse_qs(parsed.query)['email']:
        email = urlparse.parse_qs(parsed.query)['email']
        # if mail:
        print email


        patiente = patient.objects.filter(email = email)
        print patiente
        data = []
        i = 0
        for obj in patiente:
            data.append([])
            data[i] = {
                "id": obj.id,
                "name": obj.name,
                "email": obj.email,
                "phone": obj.phone
            }
            i += 1
        return HttpResponse(json.dumps(data), content_type="application/json")

    else:

        patientl = patient.objects.all()
        data = []

        # logger.error('tada')

        i = 0
        for obj in patientl:
            data.append([])
            data[i] = {
                "id": obj.id,
                "name": obj.name,
                "email": obj.email,
                "phone": obj.phone
            }
            i += 1
        print data

        #return render(request, template_name, data)
        #template = loader.get_template('patientl_list.html')

        # response_data = {}
        # response_data['result'] = data
        return HttpResponse(json.dumps(data), content_type="application/json")

#def patientl_list1(request):

    #self.request.GET.get('email')
    #print email

    #if patient.objects.filter(email='email'):

       #print email




def doctor(request):
    #if post request came
    if request.method == 'POST':


        #getting values from post
          name = request.POST.get('name')
          designation = request.POST.get('designation')
          form = doctorForm(request.POST)
          if form.is_valid():
              #f = doctor(name='name', designation='designation')
              f = form.save()





          template = loader.get_template('index1.html')

          return HttpResponse(template.render(request))
    else:
        #if post request is not true
        #returing the form template
        template = loader.get_template('index1.html')
        return HttpResponse(template.render())
